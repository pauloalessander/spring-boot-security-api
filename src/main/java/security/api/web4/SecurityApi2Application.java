package security.api.web4;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import security.api.web4.model.Magazine;
import security.api.web4.model.Role;
import security.api.web4.model.RoleEnum;
import security.api.web4.repository.RoleRepository;
import security.api.web4.service.MagazineService;
import security.api.web4.service.UserService;

@SpringBootApplication
public class SecurityApi2Application {

	public static void main(String[] args) {
		SpringApplication.run(SecurityApi2Application.class, args);
	}
	
	@Bean
    CommandLineRunner init(MagazineService magazineService, UserService userService, RoleRepository roleRepository) {
        return args -> {
        	magazineService.save(new Magazine("Veja", 15.41));
        	magazineService.save(new Magazine("AutoSport", 9.69));
        	magazineService.save(new Magazine("Caras", 47.99));
        	
        	roleRepository.save(new Role(RoleEnum.ADMIN));
        	roleRepository.save(new Role(RoleEnum.USER));
        	roleRepository.save(new Role(RoleEnum.MODERATOR));
        };
    }

}
