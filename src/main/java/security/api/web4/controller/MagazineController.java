package security.api.web4.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import security.api.web4.model.Magazine;
import security.api.web4.service.MagazineService;

@RestController
@RequestMapping("/magazine")
public class MagazineController {

	@Autowired
	private MagazineService magazineService;
	
	@GetMapping
    public ResponseEntity<List<Magazine>> list() {
        return ResponseEntity.ok().body(magazineService.list());
    }
	
	@PostMapping
	public ResponseEntity<?> save(@RequestBody Magazine magazine) {
		Magazine saved = magazineService.save(magazine);
		if (saved == null)
			return ResponseEntity.badRequest().build();
		return ResponseEntity.ok().build();
		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> remove(@PathVariable Integer id) {
		Magazine mag = magazineService.search(id);
		if (mag == null)
			return ResponseEntity.notFound().build();
		
		magazineService.remove(id);
		return ResponseEntity.ok().build();
	}
}
