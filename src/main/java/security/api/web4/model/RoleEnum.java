package security.api.web4.model;

public enum RoleEnum {

	USER, MODERATOR, ADMIN
}
