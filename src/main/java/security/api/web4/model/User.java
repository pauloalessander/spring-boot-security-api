package security.api.web4.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;

@Entity
@Table(name = "users")
public class User {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;    
	
	@Column(unique = true)
    private String username;
	
	@Column(unique = true)
	@Email
    private String email;
    private String password;
	
    @ManyToMany
    private Set<Role> roles = new HashSet<>();
    
    public User() {
    	
    }
	
	public User(String username, String password, String email) {
		this.username = username;
		this.password = password;
		this.email = email;
	}   
    
    public String getPassword() {
        return this.password;
    }    
    
    public String getUsername() {
        return this.username;
    }
    
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
    public Set<Role> getRoles() {
    	return roles;
    }

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}