package security.api.web4.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import security.api.web4.model.Magazine;

public interface MagazineRepository extends JpaRepository<Magazine, Integer> {

}

