package security.api.web4.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import security.api.web4.model.Role;
import security.api.web4.model.RoleEnum;

public interface RoleRepository extends JpaRepository<Role, Integer> {

	Optional<Role> findByName(RoleEnum name);
}
