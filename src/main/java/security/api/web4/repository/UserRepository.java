package security.api.web4.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import security.api.web4.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	Optional<User> findByUsername(String username);
	Boolean existsByUsername(String username);
	Boolean existsByEmail(String email);
}
