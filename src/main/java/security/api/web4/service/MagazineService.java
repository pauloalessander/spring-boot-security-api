package security.api.web4.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import security.api.web4.model.Magazine;
import security.api.web4.repository.MagazineRepository;

@Service
public class MagazineService {

	@Autowired
	private MagazineRepository magazineRepository;
	
	public List<Magazine> list() {
		return magazineRepository.findAll();
	}
	
	public Magazine save(Magazine magazine) {
		return magazineRepository.save(magazine);
	}
	
	public Magazine search(Integer id) {
		Optional<Magazine> mag = magazineRepository.findById(id);
		if (mag.isEmpty())
			return null;
		return mag.get();
	}
	
	public void remove(Integer id) {
		Magazine mag = this.search(id);
		if (mag != null)
			magazineRepository.delete(mag);
	}
}
