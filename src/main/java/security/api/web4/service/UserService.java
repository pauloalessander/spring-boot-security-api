package security.api.web4.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import security.api.web4.model.User;
import security.api.web4.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;
	
	public void save(User user) {
		userRepository.save(user);
	}
	
	public boolean existsByEmail(String email) {
		return this.userRepository.existsByEmail(email);
	}
	
	public boolean existsByUsername(String username) {
		return this.userRepository.existsByUsername(username);
	}
}
