# importing libraries
import requests
import ast

# defining the api-endpoint
magazine_endpoint = "http://localhost:8000/magazine"
delete_endpoint = "http://localhost:8000/magazine/1"
signin_endpoint = "http://localhost:8000/auth/signin"
signup_endpoint = "http://localhost:8000/auth/signup"
actuator_endpoint = "http://localhost:8000/actuator"

mod = {
    "username": "mod",
    "email": "mod@ifpe.br",
    "password": "1234",
    "role": ["mod", "user"]
}
admin = {
    "username": "admin",
    "email": "admin@ifpe.br",
    "password": "admin",
    "role": ["admin", "user"]
}
user = {
    "username": "user",
    "email": "user@ifpe.br",
    "password": "123",
    "role": ["user"]
}


# SIGNUP
def sign_up(endpoint, json):
    r = requests.post(url=endpoint, json=json)
    status = r.status_code
    answer = r.text
    return answer, status


sign_up(signup_endpoint, user)
sign_up(signup_endpoint, admin)
sign_up(signup_endpoint, mod)

admin_login = {
    "username": "admin",
    "password": "admin"
}
mod_login = {
    "username": "mod",
    "password": "1234"
}
user_login = {
    "username": "user",
    "password": "123"
}


# SIGNIN
def sign_in(endpoint, json):
    r = requests.post(url=endpoint, json=json)
    status = r.status_code
    answer = r.text
    return answer, status


ans, code = sign_in(signin_endpoint, admin_login)
data = ast.literal_eval(ans)

headers = {
    "Authorization": data['tokenType'] + " " + data['token']
}
print(headers)


# GET MAGAZINES
def get_magazines(endpoint, header):
    r = requests.get(url=endpoint, headers=header)
    status = r.status_code
    answer = r.text
    return answer, status


ans, code = get_magazines(magazine_endpoint, headers)
print("Return: %s" % ans)
print("Status code: %d" % code)


# GET ACTUATOR (ADMIN ONLY)
def get_actuator(endpoint, header):
    r = requests.get(url=endpoint, headers=header)
    status = r.status_code
    answer = r.text
    return answer, status


ans, code = get_actuator(actuator_endpoint, headers)
print("Return: %s" % ans)
print("Status code: %d" % code)


# DELETE (ADMIN ONLY)
def delete_magazine(endpoint, header):
    r = requests.delete(url=endpoint, headers=header)
    status = r.status_code
    answer = r.text
    return answer, status


ans, code = delete_magazine(delete_endpoint, headers)
print("Return: %s" % ans)
print("Status code: %d" % code)

magazine = {
    "name": "Placar",
    "price": 12.00
}


# POST (ADMIN, MODERATOR)
def save_magazine(endpoint, header, json):
    r = requests.post(url=endpoint, headers=header, json=json)
    status = r.status_code
    answer = r.text
    return answer, status


ans, code = save_magazine(magazine_endpoint, headers, magazine)
print("Return: %s" % ans)
print("Status code: %d" % code)
